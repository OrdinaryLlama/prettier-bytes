#[cfg(test)]
mod tests;

// this formats out "bytes" using the most compact form, up until GB, then it just uses GB
// when formatting a type larger than a byte, this will always use exactly 5 characters for the value
// example:  34.64345 GB will be formatted as "34.64 GB", and 0.2832 GB will be "0.283 GB"
pub fn bytes_to_any_or_gb(bytes: u64) -> String {
    if bytes < 1000 {
        return format!("{} B", bytes);
    }
    let mut fbytes = bytes as f64;
    fbytes /= 1024.0;
    if fbytes < 1000.0 {
        return format!("{:.*} KB", get_precision(fbytes), fbytes);
    }
    fbytes /= 1024.0;
    if fbytes < 1000.0 {
        return format!("{:.*} MB", get_precision(fbytes), fbytes);
    }
    fbytes /= 1024.0;
    if fbytes < 1000.0 {
        return format!("{:.*} GB", get_precision(fbytes), fbytes);
    } else {
        return format!("{:.0} GB", fbytes);
    }
}

fn get_precision(num: f64) -> usize {
    if num < 10.0 {
        3
    } else if num < 100.0 {
        2
    } else {
        1
    }
}