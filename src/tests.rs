use ::*;

#[test]
fn basic_test() {
    assert_eq!(bytes_to_any_or_gb(0), "0 B");
    assert_eq!(bytes_to_any_or_gb(123), "123 B");
    assert_eq!(bytes_to_any_or_gb(1010), "0.986 KB");
    assert_eq!(bytes_to_any_or_gb(83724), "81.76 KB");
    assert_eq!(bytes_to_any_or_gb(1236974), "1.180 MB");
    assert_eq!(bytes_to_any_or_gb(987234684320), "919.4 GB");
    assert_eq!(bytes_to_any_or_gb(239874652349823), "223401 GB");
}